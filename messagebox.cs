using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace messagebox
{
    class Program
    {

        [DllImport("User32.dll")]
        public static extern int MessageBoxW(
            IntPtr hWnd,
            [param: MarshalAs(UnmanagedType.LPWStr)] string lpText,
            [param: MarshalAs(UnmanagedType.LPWStr)] string lpCaption,
            UInt32 uType
        );


        static void Main(string[] args)
        {
            MessageBoxW(IntPtr.Zero, "Hello World", "My Title", 0);

            Console.ReadKey();
        }
    }
}