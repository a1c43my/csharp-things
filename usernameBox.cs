using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

/*
 int MessageBoxW(
  [in, optional] HWND    hWnd,
  [in, optional] LPCWSTR lpText,
  [in, optional] LPCWSTR lpCaption,
  [in]           UINT    uType
);
*/

/*
  BOOL GetUserNameA(
  [out]     LPSTR   lpBuffer,
  [in, out] LPDWORD pcbBuffer
);
 * */
namespace messagebox
{
    class Program
    {

        [DllImport("User32.dll")]
        public static extern int MessageBoxW(
            IntPtr hWnd,
            [param: MarshalAs(UnmanagedType.LPWStr)] string lpText,
            [param: MarshalAs(UnmanagedType.LPWStr)] string lpCaption,
            UInt32 uType
        );

        [DllImport("Advapi32.dll")]
        public static extern bool GetUserNameW(
            [param: MarshalAs(UnmanagedType.LPWStr)] StringBuilder lpBuffer,
            ref UInt32 pcbBuffer
        );


        static void Main(string[] args)
        {
            //MessageBoxW(IntPtr.Zero, "Hello World", "My Title", 4);
             
            StringBuilder mySB = new StringBuilder(100); //allocate memory of size 100
            UInt32 sZ = 20;
            
            bool res = GetUserNameW(mySB, ref sZ);
            string user = mySB.ToString();
            //Console.WriteLine(user);
            MessageBoxW(IntPtr.Zero, "Your user is "+user, "Username Box", 4);

            Console.ReadKey();
        }
    }
}