using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_tutorial
{
    /*
      #region [name]
      #endregion
    */
    internal class Program
    {
        static void Main(string[] args)
        {
            //region is used to make it collapsable 
            #region comments section
            #endregion

            #region variables
            /*
             int is 4 bytes 
             char is 1 byte
             string is 
             double is 
            */

            int year = 2022;
            int inc = 1;
            //int dec = 1;
            string msg1 = "\nEOX\\00\n";
            #endregion

            #region basic i/o

            Console.WriteLine("First C-sharp\n. . .\nThe year is {0}\nnext year is {1}", year, inc + year);
            Console.WriteLine("Chck {0}", inc); // using format to fill values
            #endregion

            #region loops

            int i, t;
            for (i = 0; i < 5; i++)
            {
                Console.Write(i); // same line, use WriteLine to auto newline

            }
            t = i;
            while (t < (i * 3) + 1)
            {
                Console.WriteLine(t);
                t++;
            }
            #endregion
            //stop the program for user keypress
            #region end program


            Console.WriteLine("{0}", msg1);
            Console.ReadKey();
            #endregion
        }
    }

}
